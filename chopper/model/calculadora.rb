class Calculadora
  STATE = {'CA' => 0.0825,
          'NV' => 0.08,
          'TX' => 0.0625,
          'AL' => 0.04,
          'UT' => 0.0685}

  def calcular(quantity, price, state)
    order_value = price * quantity

    
    if order_value > 1000
      puts("{} no soportado", order_value)
      return
    end

    if ! STATE.key?(state)
      puts("state {} no soportado", state)
      return
    end

    taxes = STATE[state]
    if order_value <= 1000
      discount = 0.03
    elsif order_value <= 5000
      discount = 0.05
    elsif order_value <= 7000
      discount = 0.07
    elsif order_value <= 10000
      discount = 0.1
    else
      discount = 0.15
    end
    
    puts((order_value) + order_value*taxes - order_value*discount)
  end
end

calculadora = Calculadora.new
quantity = ARGV[0].to_i
price = ARGV[1].to_i
state = ARGV[2]
calculadora.calcular(quantity, price, state)